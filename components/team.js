import React from "react";
import Image from "next/image";
export default function Team() {
    return (
        <>
    <div className="w-full bg-[#0e0129] h-80 text-white">
          <div className=" pt-20 pb-36 text-center font-bold ">
            <h1 className="text-[40px] mb-4 font-sans">Team</h1>
            <div>
              <a className="px-3 font-medium">Home</a>
              <span className="px-3 text-[#d80650] text-base font-medium">Team</span>
            </div>
          </div>
        </div>

        <div className="container mx-auto lg:px-28 sm:px-8 md:px-2 sm:pb-5 md:pb-5 sm:pt-10 lg:pb-10">
          <h1 className="text-black font-sans font-extrabold text-3xl text-center sm:pb-4 lg:px-44 sm:px-12 lg:pt-20">
            Our Expert Team
          </h1>
          <p className="text-black font-sans text-center sm:px-2 md:px-6 lg:px-48 lg:pt-5">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit.
            Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita
            veritatis voluptates minima sapiente.
          </p>
        </div>


       <div className="lg:px-28">
          <div className="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-2 -mx-2 mt-8 lg:-mb-20">
          <div className=" px-4 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
              <Image src="/images/img1.jpg" width="546" height="700"></Image>

              <div className="bg-white bg-400 h-28 lg:mb-20 md:mb-20 sm:mb-20 shadow-2xl shadow-gray-300">
                <div className="max-w-sm rounded overflow-hidden">
                  <div className="px-6 py-4">
                    <h1 className="font-bold text-lg text-center my-1">John Smith</h1>
                    <p className="font-bold text-md text-[#d80650] text-center my-1">Web Developer</p>
                  </div>
                </div>
              </div>
            </div>
            <div className=" px-4 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
              <Image src="/images/img2.jpg" width="546" height="700"></Image>

              <div className="bg-white bg-400 h-28 lg:mb-20 md:mb-20 sm:mb-20 shadow-2xl shadow-gray-300">
                <div className="max-w-sm rounded overflow-hidden">
                  <div className="px-6 py-4">
                    <h1 className="font-bold text-lg text-center my-1">Sarah Swift</h1>
                    <p className="font-bold text-md text-[#d80650] text-center my-1">Executive</p>
                  </div>
                </div>
              </div>
            </div>
            <div className=" px-4 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
              <Image src="/images/img3.jpg" width="546" height="700"></Image>

              <div className="bg-white bg-400 h-28 lg:mb-20 md:mb-20 sm:mb-20 shadow-2xl shadow-gray-300">
                <div className="max-w-sm rounded overflow-hidden">
                  <div className="px-6 py-4">
                    <h1 className="font-bold text-lg text-center my-1">Alita Scot</h1>
                    <p className="font-bold text-md text-[#d80650] text-center my-1">Programmer</p>
                  </div>
                </div>
              </div>
            </div>
            
            <div className=" px-4 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
              <Image src="/images/img4.jpg" width="546" height="700"></Image>

              <div className="bg-white bg-400 h-28 mb-48 shadow-2xl shadow-gray-300">
                <div className="max-w-sm rounded overflow-hidden">
                  <div className="px-6 py-4">
                    <h1 className="font-bold text-lg text-center my-1">Denial James</h1>
                    <p className="font-bold text-md text-[#d80650] text-center my-1">CEO</p>
                  </div>
                </div>
              </div>
            </div>
        


            <div className="px-4 lg:-mt-32 sm:-mt-20 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
              <Image src="/images/img5.jpg" width="546" height="700"></Image>
              <div className="bg-white h-28 -left-4 -top-4 shadow-2xl shadow-gray-300">
                <div className="max-w-sm rounded overflow-hidden ">
                  <div className="px-6 py-4">
                    <h1 className="font-bold text-lg text-center my-1">Killv Smith</h1>
                    <p className="font-bold text-md text-[#d80650] text-center my-1">Regional Leader </p>
                  </div>
                </div>
              </div>
            </div>
            <div className=" px-4 lg:-mt-32 sm:-mt-20 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
              <Image src="/images/img6.jpg" width="546" height="700"></Image>

              <div className="bg-white bg-400 h-28 lg:mb-48 md:mb-20 sm:mb-20 shadow-2xl shadow-gray-300">
                <div className="max-w-sm rounded overflow-hidden">
                  <div className="px-6 py-4">
                    <h1 className="font-bold text-lg text-center my-1">Peter Pers</h1>
                    <p className="font-bold text-md text-[#d80650] text-center my-1">Director</p>
                  </div>
                </div>
              </div>
            </div>
            
         
            <div className=" px-4 lg:-mt-32 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
              <Image src="/images/img7.jpg" width="546" height="700"></Image>

              <div className="bg-white bg-400 h-28 lg:mb-48 md:mb-20 sm:mb-20 shadow-2xl shadow-gray-300">
                <div className="max-w-sm rounded overflow-hidden">
                  <div className="px-6 py-4">
                    <h1 className="font-bold text-lg text-center my-1">Zinkel Dew</h1>
                    <p className="font-bold text-md text-[#d80650] text-center my-1">Graphics Designer</p>
                  </div>
                </div>
              </div>
            </div>
            <div className=" px-4 lg:-mt-32 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
              <Image src="/images/img8.jpg" width="546" height="700"></Image>

              <div className="bg-white bg-400 h-28 lg:mb-48 md:mb-20 sm:mb-20  shadow-2xl shadow-gray-300">
                <div className="max-w-sm rounded overflow-hidden">
                  <div className="px-6 py-4">
                    <h1 className="font-bold text-lg text-center my-1">Thomas Olsen</h1>
                    <p className="font-bold text-md text-[#d80650] text-center my-1">Founder & CEO</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        </>
    );
}