import React from "react";
export default function Pricing() {
    return (

        <div className="">
          <div className="w-full bg-[#0e0129] text-white">
            <div className="pt-20 pb-36 text-center font-bold ">
              <h1 className="text-[40px] mb-4 font-sans">Pricing</h1>
              <div>
                <a className="px-3 lg:font-medium">Home</a>
                <span className="px-3 text-[#d80650] text-base font-medium">Pricing</span>
              </div>
            </div>
          </div>
  
          <div className="container mx-auto sm:py-10 lg:px-28 md:px-2 md:py-10">
            <h1 className="text-black font-sans font-bold lg:text-3xl sm:text-2xl text-center sm: lg:px-44 lg:pt-10">
              Buy Our Plans & Packages Monthly
            </h1>
            <p className="text-black font-sans text-center lg:px-48 lg:pt-5 sm:px-10 sm:pt-5 md:px-2">
              Lorem, ipsum dolor sit amet consectetur adipisicing elit.
              Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita
              veritatis voluptates minima sapiente.
            </p>
          </div>
          <div className="sm:px-52 md:px-60 lg:px-28">
            <div className="lg:flex lg:-mx-2 lg:my-8 md:px-10 sm:-mx-2 sm:my-3">
              <div className="lg-w-1/3 sm:w-72 md:w-96 lg:px-3 sm:px-3 md:px-3 sm:pb-6 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
                <div className="bg-[#fef4f8] bg-400 h-36 shadow-2xl shadow-white">
                  <div className="max-w-sm rounded overflow-hidden">
                    <div className="px-6 py-4"> 
                      <h1 className="font-bold text-2xl text-center my-3">One Time</h1>
                      <p className="font-bold text-4xl text-[#d80650] text-center my-4">Free <span className="text-xs text-gray-400">/Per Month</span></p>
                    </div>
                  </div>
                </div>
                <div className="bg-white shadow-lg h-96">
                  <div className=" border-b border-pink-100 rounded h-16">
                    <p className="font-semibold text-center text-base pt-6">The Departure of the Expect
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Remote Administrator
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base  py-3">Configure Software
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-gray-400 text-center text-base py-3">Special Application
                    </p>
                  </div>
                  <div className="rounded h-14">
                    <p className="font-semibold text-gray-400 text-center text-base py-3">24/7 Support
                    </p>
                  </div>
                  <div className="lg:h-20 sm:px-16 sm:py-2 md:px-28 lg:px-20 lg:py-2">
                    <button className="btn bg-[#d80650] sm:px-4 sm:py-3 lg:px-8 lg:py-3 sm:rounded-none text-white hover:bg-black">Save Changes</button>
                  </div>
                </div>
              </div>
              <div className="lg-w-1/3 sm:w-72 md:w-96 lg:px-3 sm:px-3 md:px-3 sm:pb-6 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
                <div className="bg-[#fef4f8] bg-400 h-36 shadow-2xl shadow-white">
                  <div className="max-w-sm rounded overflow-hidden">
                    <div className="px-6 py-4">
                      <h1 className="font-bold text-2xl text-center my-3">Business</h1>
                      <p className="font-bold text-4xl text-[#d80650] text-center my-4">$70 <span className="text-xs text-gray-400">/Per Month</span></p>
                    </div>
                  </div>
                </div>
                <div className="bg-white shadow-lg h-96">
                  <div className=" border-b border-pink-100 rounded h-16">
                    <p className="font-semibold text-center text-base pt-6">The Departure of the Expect
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Remote Administrator
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base  py-3">Configure Software
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Special Application
                    </p>
                  </div>
                  <div className="rounded h-14">
                    <p className="font-semibold text-gray-400 text-center text-base py-3">24/7 Support
                    </p>
                  </div>
                  <div className="lg:h-20 sm:px-16 sm:py-2 md:px-28 lg:px-20 lg:py-2">
                    <button className="btn bg-[#d80650] sm:px-4 sm:py-3 lg:px-8 lg:py-3 sm:rounded-none text-white hover:bg-black">Save Changes</button>
                  </div>
                </div>
              </div>
              <div className="lg-w-1/3 sm:w-72 md:w-96 lg:px-3 sm:px-3 md:px-3 sm:pb-6 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
                <div className="bg-[#fef4f8] bg-400 h-36 shadow-2xl shadow-white">
                  <div className="max-w-sm rounded overflow-hidden">
                    <div className="px-6 py-4">
                      <h1 className="font-bold text-2xl text-center my-3">Exclusive</h1>
                      <p className="font-bold text-4xl text-[#d80650] text-center my-4"> $120<span className="text-xs text-gray-400">/Per Month</span></p>
                    </div>
                  </div>
                </div>
                <div className="bg-white shadow-lg h-96">
                  <div className=" border-b border-pink-100 rounded h-16">
                    <p className="font-semibold text-center text-base pt-6">The Departure of the Expect
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Remote Administrator
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base  py-3">Configure Software
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Special Application
                    </p>
                  </div>
                  <div className="rounded h-14">
                    <p className="font-semibold text-center text-base py-3">24/7 Support
                    </p>
                  </div>
                  <div className="lg:h-20 sm:px-16 sm:py-2 md:px-28 lg:px-20 lg:py-2">
                    <button className="btn bg-[#d80650] sm:px-4 sm:py-3 lg:px-8 lg:py-3 sm:rounded-none text-white hover:bg-black">Save Changes</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
          <div className="container mx-auto sm:py-4 sm:pb-4 lg:px-28 md:px-2 md:py-10">
            <h1 className="text-black font-sans font-bold lg:text-3xl sm:text-2xl text-center lg:px-44 lg:pt-2">
              Buy Our Plans & Packages Yearly
            </h1>
            <p className="text-black font-sans text-center lg:px-48 lg:pt-5 sm:px-10 sm:pt-5 md:px-2">
              Lorem, ipsum dolor sit amet consectetur adipisicing elit.
              Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita
              veritatis voluptates minima sapiente.
            </p>
          </div>
          <div className="sm:px-52 md:px-60 lg:px-28">
            <div className="lg:flex lg:-mx-2 lg:my-12 md:px-10 sm:-mx-2 sm:my-3">
              <div className="lg-w-1/3 sm:w-72 md:w-96 lg:px-3 sm:px-3 md:px-3 sm:pb-6 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
                <div className="bg-[#fef4f8] bg-400 h-36 shadow-2xl shadow-white">
                  <div className="max-w-sm rounded overflow-hidden">
                    <div className="px-6 py-4">
                      <h1 className="font-bold text-2xl text-center my-3">One Time</h1>
                      <p className="font-bold text-4xl text-[#d80650] text-center my-4">$29 <span className="text-xs text-gray-400">/Per Year</span></p>
                    </div>
                  </div>
                </div>
                <div className="bg-white shadow-lg h-96">
                  <div className=" border-b border-pink-100 rounded h-16">
                    <p className="font-semibold text-center text-base pt-6">The Departure of the Expect
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Remote Administrator
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base  py-3">Configure Software
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-gray-400 text-center text-base py-3">Special Application
                    </p>
                  </div>
                  <div className="rounded h-14">
                    <p className="font-semibold text-gray-400 text-center text-base py-3">24/7 Support
                    </p>
                  </div>
                  <div className="lg:h-20 sm:px-16 sm:py-2 md:px-28 lg:px-20 lg:py-2">
                    <button className="btn bg-[#d80650] sm:px-4 sm:py-3 lg:px-8 lg:py-3 sm:rounded-none text-white hover:bg-black">Save Changes</button>
                  </div>
                </div>
              </div>
              <div className="lg-w-1/3 sm:w-72 md:w-96 lg:px-3 sm:px-3 md:px-3 sm:pb-6 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
                <div className="bg-[#fef4f8] bg-400 h-36 shadow-2xl shadow-white">
                  <div className="max-w-sm rounded overflow-hidden">
                    <div className="px-6 py-4">
                      <h1 className="font-bold text-2xl text-center my-3">Business</h1>
                      <p className="font-bold text-4xl text-[#d80650] text-center my-4">$100 <span className="text-xs text-gray-400">/Per Year</span></p>
                    </div>
                  </div>
                </div>
                <div className="bg-white shadow-lg h-96">
                  <div className=" border-b border-pink-100 rounded h-16">
                    <p className="font-semibold text-center text-base pt-6">The Departure of the Expect
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Remote Administrator
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base  py-3">Configure Software
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Special Application
                    </p>
                  </div>
                  <div className="rounded h-14">
                    <p className="font-semibold text-gray-400 text-center text-base py-3">24/7 Support
                    </p>
                  </div>
                  <div className="lg:h-20 sm:px-16 sm:py-2 md:px-28 lg:px-20 lg:py-2">
                    <button className="btn bg-[#d80650] sm:px-4 sm:py-3 lg:px-8 lg:py-3 sm:rounded-none text-white hover:bg-black ">Save Changes</button>
                  </div>
                </div>
              </div>
              <div className="lg-w-1/3 sm:w-72 md:w-96 lg:px-3 sm:px-3 md:px-3 sm:pb-6 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-500">
                <div className="bg-[#fef4f8] bg-400 h-36 shadow-2xl shadow-white">
                  <div className="max-w-sm rounded overflow-hidden">
                    <div className="px-6 py-4">
                      <h1 className="font-bold text-2xl text-center my-3">Exclusive</h1>
                      <p className="font-bold text-4xl text-[#d80650] text-center my-4"> $199<span className="text-xs text-gray-400">/Per Year</span></p>
                    </div>
                  </div>
                </div>
                <div className="bg-white shadow-lg h-96">
                  <div className=" border-b border-pink-100 rounded h-16">
                    <p className="font-semibold text-center text-base pt-6">The Departure of the Expect
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Remote Administrator
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base  py-3">Configure Software
                    </p>
                  </div>
                  <div className=" border-b border-pink-100 rounded h-14">
                    <p className="font-semibold text-center text-base py-3">Special Application
                    </p>
                  </div>
                  <div className="rounded h-14">
                    <p className="font-semibold text-center text-base py-3">24/7 Support
                    </p>
                  </div>
                  <div className="lg:h-20 sm:px-16 sm:py-2 md:px-28 lg:px-20 lg:py-2">
                    <button className="btn bg-[#d80650] sm:px-4 sm:py-3 lg:px-8 lg:py-3 sm:rounded-none text-white hover:bg-black">Save Changes</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
       
      </div>

    );
}
