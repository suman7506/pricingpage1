import React from "react";
/*import {
  IoMailSharp,
  IoLocationSharp,
  IoChevronForwardSharp,
  IoCall,
  IoSearch,
  IoLogoInstagram,
  IoLogoTwitter,
  IoLogoFacebook,
  IoLogoLinkedin,
} from "react-icons/io5";*/
import styles from '../styles/Home.module.css'

export default function Header() {
  return (
    <>
      <div className="px-5 py-4 bg-[#0e0129] main-div top_header">
        <div className="lg:flex lg:justify-between lg:items-center">
          <div className="">
            <ul className="lg:flex lg:justify-start items-center info_div">
              <li className="text-white  px-2 flex items-center lg:justify-between">

                <span className="px-1">Email : company@name.com</span>
              </li>
              <li className="text-white  px-2 flex items-center lg:justify-between">

                <span className="px-1">Mira Bhyandar 400001</span>
              </li>
              <li className="text-white  px-2 flex items-center lg:justify-between">

                <span className="px-">+ 312545235</span>
              </li>
            </ul>
          </div>
          <div className="">
            <ul className="flex lg:justify-end justifu-center items-center social_div">
              <li className="text-white px-2">

              </li>
              <li className="text-white px-2">

              </li>
              <li className="text-white px-2">

              </li>
              <li className="text-white px-2">

              </li>
            </ul>
          </div>
        </div>
      </div>
      <nav className="navbar fixed-top px-5 py-6 flex justify-between items-center py-3 bg-[#0e0129] sticky top-0 z-50">
        <a href="#">
          <h2 className="text-white text-4xl font-extrabold lg:px-2">Geecon</h2>
        </a>
        <ul className="hidden lg:flex lg:items-center lg:w-auto lg:space-x-12">
          <li className="group relative has-child text-white  font-bold">
            <a href="#">Home</a>
          </li>
          <li className="text-white  font-bold">
            <a href="#">About</a>
          </li>
          <li className="group relative has-child text-white  font-bold">
            <a href="#">Pages</a>
          </li>
          <li className="group relative has-child text-white  font-bold">
            <a href="#">Services</a>
          </li>
          <li className="text-white  font-bold">
            <a href="#">Shop</a>
          </li>
          <li className="group relative text-white  font-bold">
            <a href="#">Blog</a>
          </li>
          <li className="text-white  font-bold">
            <a href="#">Contact</a>
          </li>
        </ul>
        <div className="lg:flex lg:items-center lg:justify-between">
          <button className="btn pr-5 text-white text-3xl hover-up-2">

          </button>
          <button className="btn bg-[#d80650] px-8 py-3 rounded-lg text-white  hover-up-2">
            Get a Quotes
          </button>
        </div>
      </nav>
    </>
  );
}
