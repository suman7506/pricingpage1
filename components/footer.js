import React from "react";
//import { MdOutlineDoubleArrow } from "react-icons/md";
export default function Footer() {
  return (
    <>
      <footer className="bg-cover bg-no-repeat bg-center relative overflow-hidden z-10 before:absolute before:bg-[#241531] before:top-0 before:left-0 before:w-full before:h-full p-24 before:-z-10">
        <div className="grid grid-cols-4 gap-4">
          <div>
            <p className="text-white text-2xl font-bold underline underline-offset-1 mb-4">
              Contact Info
            </p>
            <ul className="space-y-3 text-white">
              <li className="font-bold">
                Hotline <p className="font-normal">Phone: +61-821-456</p>
              </li>
              <li className="font-bold">
                Hotline <p className="font-normal">Phone: +61-821-456</p>
              </li>
              <li className="font-bold">
                Hotline <p className="font-normal">Phone: +61-821-456</p>
              </li>
            </ul>
          </div>
          <div>
            <p className="text-white text-2xl font-bold underline underline-offset-1 mb-4">
              Services Links
            </p>
            <ul className="text-white space-y-3">
              <li className="flex items-center">
                
                <span className="px-2">Threat Hunter</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Incident Responder</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Secure Managed IT</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Compliance</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Cyber Security</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Disaster Planning</span>
              </li>
            </ul>
          </div>
          <div>
            <p className="text-white text-2xl font-bold underline underline-offset-1 mb-4">
              Quick Support
            </p>
            <ul className="text-white space-y-3">
              <li className="flex items-center">
                
                <span className="px-2">Threat Hunter</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Incident Responder</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Secure Managed IT</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Compliance</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Cyber Security</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Disaster Planning</span>
              </li>
            </ul>
          </div>
          <div>
            <p className="text-white text-2xl font-bold underline underline-offset-1 mb-4">
              Quick Links
            </p>
            <ul className="text-white space-y-3">
              <li className="flex items-center">
                
                <span className="px-2">Threat Hunter</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Incident Responder</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Secure Managed IT</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Compliance</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Cyber Security</span>
              </li>
              <li className="flex items-center">
                
                <span className="px-2">Disaster Planning</span>
              </li>
            </ul>
          </div>
        </div>
      </footer>
      <div className="px-24 py-4 bg-[#0e0129] bottom-footer">
        <div className="lg:flex lg:justify-between lg:items-center">
          <p className="text-white">
            Copyright @2022 Geecon. All Rights Reserved.
          </p>
          <p className="text-white">Terms & Conditions Privacy Policy</p>
        </div>
      </div>
    </>
  );
}
