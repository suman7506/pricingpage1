
import Header from "../components/header";
import Footer from "../components/footer";
import Team from "../components/team";
export default function team() {
  return (

    <div>
      <Header />
       <Team />
      <Footer />
    </div>

  );
}